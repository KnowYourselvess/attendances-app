# -*- coding: utf-8 -*-
""" Forms for the attendances application. """
# standard library

# django
from django import forms
from django.utils import timezone

# models
from .models import Attendance

# views
from base.forms import BaseModelForm


class AttendanceForm(BaseModelForm):
    """
    Form Attendance model.
    """

    class Meta:
        model = Attendance
        exclude = ()

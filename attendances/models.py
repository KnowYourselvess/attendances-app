# -*- coding: utf-8 -*-
""" Models for the attendances application. """
# standard library

# django
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

# models
from base.models import BaseModel
from users.models import User
from .managers import AttendanceQuerySet


class Attendance(BaseModel):
    """
    TODO: Fill this description
    The attendances system is used to store attendance.
    """

    # foreign keys
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=_("user"),
    )
    # required fields
    date = models.DateField(
        _("attendance date"),
        default=timezone.now,
        help_text=_("The date of the attendance"),
    )

    hour_entry = models.TimeField(
        _("entry hour"),
        default=timezone.now,
        help_text=_("The entry hour of the attendance"),
    )

    hour_exit = models.TimeField(
        _("exit hour"),
        default=timezone.now,
        help_text=_("The exit hour of the attendance"),
    )
    # optional fields

    # Manager
    objects = AttendanceQuerySet.as_manager()

    class Meta:
        verbose_name = _("attendance")
        verbose_name_plural = _("attendances")
        ordering = ["-id"]

    def __str__(self):
        # TODO this is an example str return, change it
        return str(self.date)

    def get_absolute_url(self):
        """ Returns the canonical URL for the Attendance object """
        # TODO this is an example, change it
        return reverse("attendance_detail", args=(self.pk,))

    def clean(self):
        if self.hour_entry > timezone.now().time():
            raise ValidationError(_("entry hour can't be in the future"))
        if self.hour_exit > timezone.now().time():
            raise ValidationError(_("exit hour can't be in the future"))
        if self.hour_entry > self.hour_exit:
            raise ValidationError(_("entry hour can't be after exit hour"))

"""
Tests for the attendances app
"""
from django.core.exceptions import ValidationError
from django.utils import timezone
from base.tests import BaseTestCase


class AttendancesTest(BaseTestCase):
    def test_entry_hour_cant_be_in_the_future(self):
        """
        Test that the entry hour of an attendance can't be in the future
        """
        future_date = (timezone.now() + timezone.timedelta(hours=5)).time()
        other_date = (timezone.now() + timezone.timedelta(hours=6)).time()
        attendance = self.create_attendance(
            user=self.user, hour_entry=future_date, hour_exit=other_date
        )
        self.assertRaises(ValidationError, attendance.clean)

    def test_entry_hour_cant_be_after_exit_hour(self):
        """
        Test that the entry hour of an attendance can't be after the exit hour
        """
        entry_hour = (timezone.now() - timezone.timedelta(hours=5)).time()
        exit_hour = (timezone.now() - timezone.timedelta(hours=6)).time()
        attendance = self.create_attendance(
            user=self.user, hour_entry=entry_hour, hour_exit=exit_hour
        )
        self.assertRaises(ValidationError, attendance.clean)

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


def validate_attendance(attendance):
    if attendance.hour_entry > attendance.hour_exit:
        raise ValidationError(_("entry hour can't be after exit hour"))
